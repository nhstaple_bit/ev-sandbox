# README #
## EyeVocab Beta Sandbox##
TODO description

* Link to [alpha](https://bitbucket.org/cdcoulon/eyevocab-proto/src/master/)
* Link to [beta](https://bitbucket.org/cdcoulon/ev-core/src/master/)

### Installation ###
TODO description

The usual NodeJS stuff.

### Contact Info ###
| Position | Name | Contact |
| :----    | :--- | :---    |
| Lead Programmer | Christian Coulon | [cdcoulon@bitbucket](https://bitbucket.org/cdcoulon) |
| Programmer | Nick Stapleton | [nhstaple_bit@bitbucket](https://bitbucket.org/nhstaple_bit/) |

### Ownership ###
University of California, Davis

Language Center

Distinguished Professor Robert J. Blake (Emeritus)
